import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ItemsApiController } from './items-api/items-api.controller';
import { ItemsApiService } from './items-api/items-api.service';

@Module({
  imports: [],
  controllers: [AppController, ItemsApiController],
  providers: [AppService, ItemsApiService],
})
export class AppModule {}
