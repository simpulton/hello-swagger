import { Controller, Get, Param, Post, Body, Put, Delete } from '@nestjs/common';
import { ItemsApiService } from './items-api.service';
import { Item } from './item.dto';

@Controller('items')
export class ItemsApiController {

  constructor(private itemsService: ItemsApiService) {}
   
  @Get()
  all(): Item[] {
    return this.itemsService.all();
  }

  @Get(':id')
  find(@Param('id') id: string): Item {
    return this.itemsService.find(id);
  }

  @Post()
  create(@Body() item: Item) {
    return this.itemsService.create(item);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() item: Item) {
    return this.itemsService.update(id, item);
  }

  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.itemsService.delete(id);
  }
}
