import { Injectable } from '@nestjs/common';
import { randomStringGenerator } from '@nestjs/common/utils/random-string-generator.util';
import { Item } from './item.dto';

let items: Item[] = [
  {
    id: '1',
    name: 'First Item',
    description: 'Item description...',
  },
  {
    id: '2',
    name: 'Second Item',
    description: 'Item description...',
  },
  {
    id: '3',
    name: 'Third Item',
    description: 'Item description...',
  },  
]

@Injectable()
export class ItemsApiService {
  all(): Item[] {
    return items;
  }

  find(id: string): Item {
    return items.find(item => item.id === id);
  }

  create(item: Item): Item {
    const newItem  = Object.assign({}, item, { id: randomStringGenerator()});
    items = [...items, newItem];
    return newItem;
  }

  update(id: string, item: Item): Item[] {
    items = items.map(itm => {
      return itm.id === id ? Object.assign({}, itm, item) : itm;
    });
    return items;
  }

  delete(id: string): Item[] {
    items = items.filter(item => item.id !== id);
    return items;
  }
}
