import { Test, TestingModule } from '@nestjs/testing';
import { ItemsApiService } from './items-api.service';

describe('ItemsApiService', () => {
  let service: ItemsApiService;
  
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ItemsApiService],
    }).compile();
    service = module.get<ItemsApiService>(ItemsApiService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
