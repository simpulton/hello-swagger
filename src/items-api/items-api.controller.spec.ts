import { Test, TestingModule } from '@nestjs/testing';
import { ItemsApiController } from './items-api.controller';

describe('ItemsApi Controller', () => {
  let module: TestingModule;
  
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [ItemsApiController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: ItemsApiController = module.get<ItemsApiController>(ItemsApiController);
    expect(controller).toBeDefined();
  });
});
